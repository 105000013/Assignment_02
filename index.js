var game = new Phaser.Game(400, 400, Phaser.AUTO, 'canvas',
    { preload: preload, create: create, update: update });

function preload(){
    game.load.image('wall', 'asset/wall.png');
    game.load.audio( 'menu','asset/menu.ogg');
}

function create(){
    leftWall = game.add.sprite(0, 0, 'wall');
    rightWall = game.add.sprite(game.width-15, 0, 'wall');
    bgMusic = game.add.audio('menu');
    bgMusic.loop = true;
    bgMusic.play();
}

function update(){

}