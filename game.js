var game = new Phaser.Game(400, 400, Phaser.AUTO, 'canvas',
    { preload: preload, create: create, update: update });

var player;
var cursor;
var platforms=[];
var lastTime=0;
var score=0;
var list;
var text4;
var text5;
var once=0;
var board=[];
var f1;
var f2;
var f3;
var s1,s2,s3;
var quit1,quit2;
var yours;
var flag2=0;
var flag3=0;
var loadingLabel;

function preload () {

    loadingLabel = game.add.text(game.width/2, 150,
        'loading...', { font: '30px Arial', fill: '#ffffff' });
        loadingLabel.anchor.setTo(0.5, 0.5);
        loadingLabel.visible=true;
        
    game.load.spritesheet('player', 'asset/player.png', 32, 32);
    game.load.image('wall', 'asset/wall.png');
    game.load.image('ceiling', 'asset/ceiling.png');
    game.load.image('normal', 'asset/platform.png');
    game.load.image('thorn', 'asset/thorn.png');
    game.load.image('gain_Life', 'asset/life.png');
    game.load.spritesheet('runRight', 'asset/runright.png', 96, 16);
    game.load.spritesheet('runLeft', 'asset/runleft.png', 96, 16);
    game.load.spritesheet('coin', 'asset/coin.png',50,68);

    game.load.audio( 'bg','asset/bg_music.ogg');
    game.load.audio( 'knock','asset/knock.ogg');
    game.load.audio( 'jumpy','asset/jumpy.ogg');
    game.load.audio( 'good','asset/good.ogg');
    game.load.audio( 'bad','asset/bad.ogg');
    game.load.audio( 'coinm','asset/coinm.ogg');
    
    game.load.spritesheet('trampoline', 'asset/trampoline.png', 96, 22);
    game.load.spritesheet('fake', 'asset/fake.png', 96, 36);

    game.load.image('pixel', 'asset/pixel.png');
}

function create () {

    loadingLabel.visible=false;
    bgMusic = game.add.audio('bg');
    bgMusic.loop = true;
    bgMusic.play();

    emitter = game.add.emitter(0, 0, 15);
// Set the 'pixel' image for the particles
    emitter.makeParticles('pixel');
    emitter.setYSpeed(-150, 150);
    emitter.setXSpeed(-150, 150);
    // Scale the particles from 2 time their size to 0 in 800ms
    // Parameters are: startX, endX, startY, endY, duration
    emitter.setScale(2, 0, 2, 0, 800);
    // Use no gravity
    emitter.gravity = 0;

    knockMusic= game.add.audio('knock');
    jumpMusic= game.add.audio('jumpy');
    goodMusic= game.add.audio('good');
    badMusic= game.add.audio('bad');
    coinMusic= game.add.audio('coinm');
     
    cursor = game.input.keyboard.addKeys({
        'enter': Phaser.Keyboard.ENTER,
        //'up': Phaser.Keyboard.UP,
        //'down': Phaser.Keyboard.DOWN,
        'left': Phaser.Keyboard.LEFT,
        'right': Phaser.Keyboard.RIGHT,
        /*'w': Phaser.Keyboard.W,
        'a': Phaser.Keyboard.A,
        's': Phaser.Keyboard.S,
        'd': Phaser.Keyboard.D*/
        'q': Phaser.Keyboard.Q
    });


    

    ceiling = game.add.image(0, 0, 'ceiling');

    leftWall = game.add.sprite(0, 0, 'wall');
    game.physics.arcade.enable(leftWall);
    leftWall.body.immovable = true;
    
    rightWall = game.add.sprite(game.width-15, 0, 'wall');
    game.physics.arcade.enable(rightWall);
    rightWall.body.immovable = true;
    createPlayer();
    list = game.add.text( game.width/2-40, 30, '排行榜', { font: '30px Arial', fill: '#66a3ff' });
    
    scoreLabel = game.add.text(30,30, 'score: 0',{ font: '18px Arial', fill: '#ffffff' });
    lifeLabel = game.add.text(game.width-60, 30, 'life: 3',{ font: '18px Arial', fill: '#ffffff' });
    // Initialize the score variable
    score = 0;
    list.visible=false;



    var i=0;
    var score_count = firebase.database().ref('/list/').orderByChild('score').limitToLast(3);
        score_count.once('value',function(snapshot){
            snapshot.forEach(function(childSnapshot){
                var text4 = childSnapshot.val().name;
                var text5 = childSnapshot.val().score;
                board[i]=text4;
                i++;
                board[i]=text5;
                i++;
            })
        })
        
        var style2 = {fill: '#fff9b7', font: 'italic bold 25px Georgia', align: 'center'};
   
    
        f1=game.add.text(100, 150,' ' , style2);
        f1.visible=false;
        f2=game.add.text(100, 120,' ' , style2);
        f2.visible=false;
        f3=game.add.text(100, 90,' ' , style2);
        f3.visible=false;
        
        s1=game.add.text(300, 150,' ' , style2);
        s1.visible=false;
        s2=game.add.text(300, 120,' ' , style2);
        s2.visible=false;
        s3=game.add.text(300, 90,' ' , style2);
        s3.visible=false;

        var style3 = {fill: '#ff0000', fontSize: '40px'}
        yours=game.add.text(200, 220,'' , style3);
        yours.visible=false;
        yours.anchor.setTo(0.5,0.5);

        var style = {fill: ' #6bfff1', font: '22px Georgia', align: 'center'};
        quit1=game.add.text(200, 280,'press Enter to play again' , style);
        quit1.visible=false;
        quit1.anchor.setTo(0.5,0.5);
        quit2=game.add.text(200, 310,'press Q to quit' , style);
        quit2.visible=false;
        quit2.anchor.setTo(0.5,0.5);

        coin = game.add.sprite(0, 0, '');
}

function update () { 

    var rand = Math.random() * 100;
    if(rand<2 && flag3==0){
        createCoin();
    }

    game.physics.arcade.overlap(player, coin,takeCoin, null, this);
    game.physics.arcade.overlap(player,platforms, checkTouch, null, this);
    
    if(player.body.y > 500 || player.life<=0){
        platforms.forEach(function(plat) {plat.destroy()});
        platforms = [];
        coin.visible=false;
        bgMusic.stop();

        quit1.visible=true;
        quit2.visible=true;
        scoreLabel.visible=false;
        lifeLabel.visible=false;
        list.visible=true;
        platforms.forEach(function(plat) {plat.destroy()});
        platforms = [];

        if(flag2==0){
            if(score<20){
                badMusic.play();
            }
            else
                goodMusic.play();
            yours.setText('Your score: '+score);
            yours.visible=true;
            flag2=1;

        }
        f1.setText(board[0]);
        f1.visible=true;
        f2.setText(board[2]);
        f2.visible=true;
        f3.setText(board[4]);
        f3.visible=true;
        s1.setText(board[1]);
        s1.visible=true;
        s2.setText(board[3]);
        s2.visible=true;
        s3.setText(board[5]);
        s3.visible=true;
        if(once==0)
            var person = prompt('Keep your record', 'Name');
        once=1;
        if(person!=null){
            var profiles = {
                name: person,
                score: score
            };  
            var Update = {};
            var newKey = firebase.database().ref().child('list').push().key;
            Update['/list/'+newKey]=profiles;
            firebase.database().ref().update(Update).then(function(){});

            if(score>=board[5]){
                board[4]=person;
                board[5]=score;
                f3.setText(board[4]);
                s3.setText(board[5]);
            }
            if(score>=board[3] && score<board[5]){
                board[2]=person;
                board[3]=score;
                f2.setText(board[0]);
                f1.setText(board[2]);
                s2.setText(board[1]);
                s1.setText(board[3]);
            }
            if(score>=board[1] && score<board[3]){
                board[0]=person;
                board[1]=score;
                f1.setText(board[0]);
                s1.setText(board[1]);
            }
        }
        if(cursor.enter.isDown){
            restart();
        }
        if(cursor.q.isDown){
            location.href='index.html'
        }
    }
    game.physics.arcade.collide(player, rightWall);
    game.physics.arcade.collide(player, leftWall);
    game.physics.arcade.collide(player, platforms);
    
    
    movePlayer();
    
    updatePlatforms();
    checkTouch(player,platforms);
    createPlatformsTime();
    lifeLabel.setText('life:' + player.life);
    scoreLabel.setText('地下' + score+'階');
    list.setText( '排行榜');
    
}

function createCoin(){
    var x = Math.random() * 250+50;
    var y = Math.random() * 250+50;
    coin = game.add.sprite(x, y, 'coin');
    coin.animations.add('coin', [0, 1, 2, 3,4,5], 6,true);
    coin.play('coin');
    game.physics.arcade.enable(coin);
    flag3=1;
}

function takeCoin(player,coin){

    coinMusic.play();
    coin.kill(); // Kill the coin.
    emitter.x = player.x;
    emitter.y = player.y;
    emitter.start(true, 800, null, 15);
// Set delay
    game.time.events.add(1000,function() {game.state.start('menu');},this);

    score += 2; 
    flag3=0;
}

function leaderboard(){
    var i=0;
    var score_count = firebase.database().ref('/list/').orderByChild('score').limitToLast(3);
        score_count.once('value',function(snapshot){
            snapshot.forEach(function(childSnapshot){
                var text4 = childSnapshot.val().name;
                var text5 = childSnapshot.val().score;
                board[i]=text4;
                i++;
                board[i]=text5;
                i++;
            })
        })
}

function checkTouch(player,platforms) {
    if(player.body.y < 0) {
        if(player.body.velocity.y < 0) {
            player.body.velocity.y = 0;
        }
        if(game.time.now > player.unable) {
            player.life -= 1;
            //game.camera.flash(0xff0000, 100);
            game.camera.shake(0.02, 300);
            player.unable = game.time.now + 2000;
        }
    }
    else if(platforms.key==0){
        if (player.touch !== platforms) {
            knockMusic.play();
            player.touch = platforms;
        }
    }
    else if(platforms.key == 1) {
        if (player.touch !== platforms) {
            player.life -= 1;
            player.touch = platforms;
            game.camera.flash(0xff0000, 100);
        }
    }
    else if(platforms.key == 2) {
        if (player.touch !== platforms) {
        
            player.life += 1;
            player.touch = platforms;
        }
    }
    else if(platforms.key == 3) {
        player.body.x += 2;
    }
    else if(platforms.key == 4) {
        player.body.x -= 2;
    }
    else if(platforms.key == 5) {
        platforms.play('jump');
        player.body.velocity.y = -250;
        player.body.y-=1;
        jumpMusic.play();
    }
    else if(platforms.key == 6) {
        if(player.touchOn !== platforms) {
            platforms.play('turn');
            knockMusic.play();
            setTimeout(function() {
                platforms.body.checkCollision.up = false;
            }, 200);
            player.touchOn = platforms;
        }
    }
    
}

function createPlayer() {
    player = game.add.sprite(game.width/2, 50, 'player');
    //player.direction = 10;
    game.physics.arcade.enable(player);
    player.body.gravity.y = 300;
    player.animations.add('left', [0, 1, 2, 3], 8);
    player.animations.add('right', [9, 10, 11, 12], 8);
    //player.animations.add('flyleft', [18, 19, 20, 21], 12);
    //player.animations.add('flyright', [27, 28, 29, 30], 12);
    //player.animations.add('fly', [36, 37, 38, 39], 12);
    player.life = 3;
    player.unable = 0;
    player.touch = 0;
}

function movePlayer () {
    
    if(cursor.left.isDown) {
        player.body.velocity.x = -200;
        player.animations.play('left');
    } else if(cursor.right.isDown) {
        player.body.velocity.x = 200;
        player.animations.play('right');
    } else {
        player.body.velocity.x = 0;
        player.frame = 8;
    }
}

function createPlatformsTime () {
    
    if(game.time.now > lastTime + 800) {
        lastTime = game.time.now;
        platform_one();
        score += 1;
    }
}

function platform_one () {

    var platform;
    var x = Math.random()*(game.width-16-16-96)+16;
    var y = 400;
    var rand = Math.random() * 100;

    if(rand < 30) {
        
        platform = game.add.sprite(x, y, 'normal');
        platform.key=0;
    }    
     else if (rand < 40) {
        platform = game.add.sprite(x, y, 'thorn');
        platform.key=1;
        game.physics.arcade.enable(platform);
        platform.body.setSize(96, 31, 0, 12);
     }else  if (rand < 50) {
        platform = game.add.sprite(x, y, 'runLeft');
        platform.animations.add('scrollL', [0, 1, 2, 3], 16, true);
        platform.play('scrollL');
        platform.key=4;
        game.physics.arcade.enable(platform);
    }  else if (rand < 60) {
        platform = game.add.sprite(x, y, 'runRight');
        platform.animations.add('scrollR', [0, 1, 2, 3], 16, true);
        platform.play('scrollR');
        platform.key=3;
        game.physics.arcade.enable(platform);
    }  else if (rand < 70) {
        platform = game.add.sprite(x, y, 'gain_Life');
        platform.key=2;
        game.physics.arcade.enable(platform);
    }   else if (rand <80) {
        platform = game.add.sprite(x, y, 'trampoline');
        platform.animations.add('jump', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
        platform.key=5;
        game.physics.arcade.enable(platform);
    } else if(rand>80){
        platform = game.add.sprite(x, y, 'fake');
        platform.animations.add('turn', [0, 1, 2, 3, 4, 5, 0], 14);
        platform.key=6;
        game.physics.arcade.enable(platform);
    }

    game.physics.arcade.enable(platform);
    platform.body.immovable = true;
    platforms.push(platform);

}
function updatePlatforms () {
    for(var i=0; i<platforms.length; i++) {
        var platform = platforms[i];
        if(score<50)
            platform.body.position.y -= 2;
        else
        platform.body.position.y -= 3;
        if(platform.body.position.y <= -10) {
            platform.destroy();
            platforms.splice(i, 1);
        }
    }
}

function restart () {
    leaderboard();
    scoreLabel.visible=true;
    lifeLabel.visible=true;
    list.visible=false;
    score = 0;
    game.time.now=0;
    lastTime=0;
    createPlayer();
    once=0;
    f1.visible=false;
    f2.visible=false;
    f3.visible=false;
    s1.visible=false;
    s2.visible=false;
    s3.visible=false;
    bgMusic.play();
    quit1.visible=false;
    quit2.visible=false;
    yours.visible=false;
    flag2=0;
    coin.visible=true;
}