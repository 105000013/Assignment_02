# Software Studio 2018 Spring Assignment 02 小朋友下樓梯

## 小朋友下樓梯
<img src="example01.png" width="700px" height="500px"></img>

## Goal
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Complete a game "小朋友下樓梯" by Phaser. (JavaScript or TypeScript)
3. Your game should reach the basic requirements.
4. You can download needed materials from some open source webpage to beautify the appearance.
5. Commit to "your" project repository and deploy to Gitlab page.
6. **Report which items you have done and describing other functions or feature in REABME.md.**

## Scoring 
|                                              Item                                              | Score |
|:----------------------------------------------------------------------------------------------:|:-----:|
| A complete game process: start menu => game view => game over => quit or play again            |  20%  |
| Your game should follow the basic rules of  "小朋友下樓梯".                                    |  15%  |
|         All things in your game should have correct physical properties and behaviors.         |  15%  |
| Set up some interesting traps or special mechanisms. .(at least 2 different kinds of platform) |  10%  |
| Add some additional sound effects and UI to enrich your game.                                  |  10%  |
| Store player's name and score in firebase real-time database, and add a leaderboard to your game.        |  10%  |
| Appearance (subjective)                                                                        |  10%  |
| Other creative features in your game (describe on README.md)                                   |  10%  |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/Assignment_02**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, .ts, etc.
    * source files
* **Deadline: 2018/05/24 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

## Other creative features
1. different kinds of platform
    - 一般platform
    - 帶刺platform(碰到的話螢幕會閃爍, 生命值減1)
    - 傳送帶platform
    - 彈簧platform
    - 假platform
    - 愛心platform(增加生命值)
2. leaderboard
    - 按照分數由最高排到最低
    - 列出前三名
3. sound effects
    - 遊戲背景音樂
    - 碰到一般及假platform的聲音
    - 碰到彈簧platform的聲音
    - menu聲音
    - 遊戲結束聲音(分成分數大於20的聲音以及分數小於20的聲音)
    - 吃金幣聲音
4. 吃金幣能加分數
5. 吃到金幣, 金幣會粉碎
6. 分數>50, 難度增加(遊戲速度變快)
7. 碰到天花板, 螢幕會晃動
8. 有loading畫面